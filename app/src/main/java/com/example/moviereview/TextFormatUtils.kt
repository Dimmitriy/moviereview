package com.example.moviereview

import com.example.moviereview.model.ListObject

class TextFormatUtils {

    fun <T : ListObject> getString(elements: List<T>): String {
        val result = StringBuffer()
        for (element in elements) {
            result
                    .append(element.value)
                    .append(", ")
        }
        val length = result.length
        result.delete(length - 2, length - 1)
        return result.toString()
    }

}
