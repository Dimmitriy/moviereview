package com.example.moviereview.model.response

import com.google.gson.annotations.SerializedName

class GetAccountResponse (@SerializedName("success") var success: Boolean,
                          @SerializedName("account_id") var account_id: String)