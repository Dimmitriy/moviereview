package com.example.moviereview.model.response

import com.google.gson.annotations.SerializedName

class RequestTokenResponse (@SerializedName("success") var success: Boolean,
                            @SerializedName("expires_at") var expires_at: String,
                            @SerializedName("request_token") var requestToken: String)