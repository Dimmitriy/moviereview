package com.example.moviereview.model

import com.google.gson.annotations.SerializedName

class ProductionCountry : ListObject {

    @SerializedName("iso_3166_1") var iso31661: String? = null
    @SerializedName("name") var name: String? = null

    override val value: String?
        get() = name

}
