package com.example.moviereview.model

import com.google.gson.annotations.SerializedName

class ProductionCompany : ListObject {

    @SerializedName("name")
    var name: String? = null

    @SerializedName("id")
    var id: Int? = null

    override val value: String?
        get() = name

}
