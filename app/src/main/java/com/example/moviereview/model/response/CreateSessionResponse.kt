package com.example.moviereview.model.response

import com.google.gson.annotations.SerializedName

class CreateSessionResponse (@SerializedName("success") var success: Boolean,
                             @SerializedName("session_id") var sessionId: String)