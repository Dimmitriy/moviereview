package com.example.moviereview.model

import com.google.gson.annotations.SerializedName

class SpokenLanguage : ListObject {

    @SerializedName("iso_639_1")
    var iso6391: String? = null

    @SerializedName("name")
    var name: String? = null

    override val value: String?
        get() = name

}
