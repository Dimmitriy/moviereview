package com.example.moviereview.search

import com.example.moviereview.favorite.model.FavoriteRepository
import com.example.moviereview.rated.model.RatedRepository
import com.example.moviereview.search.model.SearchRepository
import com.example.moviereview.watchlist.model.WatchListRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SearchMoviesPresenter
@Inject constructor(private val favoriteRepository: FavoriteRepository,
                    private val ratedRepository: RatedRepository,
                    private val watchListRepository: WatchListRepository,
                    private val searchRepository: SearchRepository) : SearchMoviesContract.Presenter {

    lateinit var view: SearchMoviesContract.View
    private val disposable = CompositeDisposable()

    override fun getSearchMovies(query: String, api_key: String) {
        disposable.add(
                searchRepository.getSearchMovies(query, api_key)
                        .compose(adjustThreads(view))
                        .subscribe({ moviesResponseModel ->
                            view.showSearchMovies(moviesResponseModel.results)
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, mediaId: Int, favorite: Boolean) {
        val body = HashMap<String, Any>()
        body["media_type"] = "movie"
        body["media_id"] = mediaId
        body["favorite"] = favorite
        disposable.add(
                favoriteRepository.markMovieAsFavorite(account_id, api_key, session_id, body)
                        .compose(adjustThreads(view))
                        .subscribe({
                            view.showMessage("movie marked to favourite code: ${it.status_code}, message: ${it.status_message}")
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, mediaId: Int, watchlist: Boolean) {
        val body = HashMap<String, Any>()
        body["media_type"] = "movie"
        body["media_id"] = mediaId
        body["watchlist"] = watchlist
        disposable.add(
                watchListRepository.markMovieToWatchList(account_id, api_key, session_id, body)
                        .compose(adjustThreads(view))
                        .subscribe({
                            view.showMessage("movie marked to watchlist code: ${it.status_code}, message: ${it.status_message}")
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun rateMovie(movie_id: Int, api_key: String, session_id: String, value: Float) {
        val body = HashMap<String, Any>()
        body["value"] = value
        disposable.add(
                ratedRepository.rateMovie(movie_id, api_key, session_id, body)
                        .compose(adjustThreads(view))
                        .subscribe({
                            view.showMessage("movie rated code: ${it.status_code}, message: ${it.status_message}")
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {
        disposable.clear()
    }

}
