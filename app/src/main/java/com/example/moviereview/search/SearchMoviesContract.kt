package com.example.moviereview.search

import com.example.moviereview.base.BasePresenter
import com.example.moviereview.base.BaseView
import com.example.moviereview.model.Movie

class SearchMoviesContract {

    interface View : BaseView {

        fun showSearchMovies(searchedMovies: List<Movie>)

    }

    interface Presenter : BasePresenter {

        fun getSearchMovies(query: String, api_key: String)

        fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, mediaId: Int, favorite: Boolean)

        fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, mediaId: Int, watchlist: Boolean)

        fun rateMovie(movie_id: Int, api_key: String, session_id: String, value: Float)

    }
}
