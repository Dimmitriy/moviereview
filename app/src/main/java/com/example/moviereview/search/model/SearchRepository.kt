package com.example.moviereview.search.model

import com.example.moviereview.model.MoviesResponseModel
import io.reactivex.Observable

interface SearchRepository {

    fun getSearchMovies(query: String, api_key: String): Observable<MoviesResponseModel>

}