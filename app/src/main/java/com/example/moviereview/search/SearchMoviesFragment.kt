package com.example.moviereview.search

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.moviereview.App
import com.example.moviereview.Constants
import com.example.moviereview.MoviesAdapter
import com.example.moviereview.R
import com.example.moviereview.di.component.DaggerPresentersComponent

import com.example.moviereview.di.module.PresentersModule
import com.example.moviereview.model.Movie
import com.example.moviereview.particular.ParticularMovieActivity
import kotlinx.android.synthetic.main.fragment_search_movies.*
import javax.inject.Inject

class SearchMoviesFragment : Fragment(), SearchMoviesContract.View, SearchView.OnQueryTextListener, View.OnClickListener {

    private var adapter: MoviesAdapter? = null

    @Inject
    lateinit var presenter: SearchMoviesPresenter

    override fun getViewContext(): Context? = super.getContext()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerPresentersComponent.builder()
                .appComponent((activity?.application as App).getAppComponent())
                .presentersModule(PresentersModule())
                .build()
                .inject(this)
        presenter.view = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = MoviesAdapter(context!!, this)
        searchMovies.adapter = adapter

        searchView.setOnQueryTextListener(this)
        searchView.queryHint = resources.getString(R.string.search_movies)
    }

    override fun onClick(v: View?) {
        startActivity(ParticularMovieActivity.newIntent(context!!, Constants.MOVIE_ID, v?.tag as Int))
    }


    override fun onResume() {
        super.onResume()
        presenter.subscribe()
    }

    override fun onPause() {
        super.onPause()
        presenter.subscribe()
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String): Boolean {
        if (newText.length >= 3) {
            presenter.getSearchMovies(newText, Constants.API_KEY)
        }
        return true
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, "search movies message: $message", Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progress!!.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress!!.visibility = View.GONE
    }

    override fun showSearchMovies(searchedMovies: List<Movie>) {
        adapter!!.setResponse(searchedMovies)
    }

}
