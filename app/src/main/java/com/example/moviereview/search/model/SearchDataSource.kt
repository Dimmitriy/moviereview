package com.example.moviereview.search.model

import com.example.moviereview.api.MoviesApiSet
import com.example.moviereview.model.MoviesResponseModel
import io.reactivex.Observable

class SearchDataSource(private val apiSet: MoviesApiSet): SearchRepository {

    override fun getSearchMovies(query: String, api_key: String): Observable<MoviesResponseModel> {
        return apiSet.getSearchMovies(query, api_key)
    }

}