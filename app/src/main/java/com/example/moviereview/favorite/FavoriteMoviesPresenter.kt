package com.example.moviereview.favorite

import com.example.moviereview.favorite.model.FavoriteRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FavoriteMoviesPresenter
@Inject constructor(private val favoriteRepository: FavoriteRepository) : FavoriteMoviesContract.Presenter {

    lateinit var view: FavoriteMoviesContract.View
    private val disposable = CompositeDisposable()

    override fun getFavoriteMovies(sort_by: String, api_key: String, session_id: String, account_id: String, page: Int) {
        disposable.add(
                favoriteRepository.getFavoriteMovies(account_id, sort_by, api_key, session_id, page + 1)
                        .compose(adjustThreads(view))
                        .subscribe({ moviesResponseModel ->
                            view.showFavoriteMovies(moviesResponseModel.results)
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, mediaId: Int, favorite: Boolean) {
        val body = HashMap<String, Any>()
        body["media_type"] = "movie"
        body["media_id"] = mediaId
        body["favorite"] = favorite
        disposable.add(
                favoriteRepository.markMovieAsFavorite(account_id, api_key, session_id, body)
                        .compose(adjustThreads(view))
                        .subscribe({
                            view.showMessage("movie marked to favourite code: ${it.status_code}, message: ${it.status_message}")
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {
        disposable.dispose()
    }

}