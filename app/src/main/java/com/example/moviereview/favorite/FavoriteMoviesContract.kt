package com.example.moviereview.favorite

import com.example.moviereview.base.BasePresenter
import com.example.moviereview.base.BaseView
import com.example.moviereview.model.Movie

class FavoriteMoviesContract {

    interface View : BaseView {

        fun showFavoriteMovies(movies: List<Movie>)

    }

    interface Presenter : BasePresenter {

        fun getFavoriteMovies(sort_by: String, api_key: String, session_id: String, account_id: String, page: Int)

        fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, mediaId: Int, favorite: Boolean)

    }

}
