package com.example.moviereview.favorite.model

import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.MarkToFavoriteResponse
import io.reactivex.Observable

interface FavoriteRepository {

    fun getFavoriteMovies(account_id: String, sort_by: String, api_key: String, session_id: String, page: Int): Observable<MoviesResponseModel>

    fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, body: HashMap<String, Any>): Observable<MarkToFavoriteResponse>

}