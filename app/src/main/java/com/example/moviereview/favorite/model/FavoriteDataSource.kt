package com.example.moviereview.favorite.model

import com.example.moviereview.api.FavoriteMoviesApiSet
import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.MarkToFavoriteResponse
import io.reactivex.Observable

class FavoriteDataSource(private val apiSet: FavoriteMoviesApiSet) : FavoriteRepository {

    override fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, body: HashMap<String, Any>): Observable<MarkToFavoriteResponse> {
        return apiSet.markMovieAsFavorite(account_id, api_key, session_id, body)
    }

    override fun getFavoriteMovies(account_id: String, sort_by: String, api_key: String,
                                   session_id: String, page: Int): Observable<MoviesResponseModel> {
        return apiSet.getFavoriteMovies(account_id, sort_by, api_key, session_id, page)
    }

}