package com.example.moviereview.favorite

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.moviereview.*
import com.example.moviereview.di.component.DaggerPresentersComponent
import com.example.moviereview.di.module.PresentersModule
import com.example.moviereview.model.Movie
import com.example.moviereview.particular.ParticularMovieActivity
import kotlinx.android.synthetic.main.fragment_favorite.*
import javax.inject.Inject

class FavoriteMoviesFragment : Fragment(), FavoriteMoviesContract.View, View.OnClickListener,
        RecyclerItemTouchHelper.RecyclerItemTouchHelperListener  {

    private lateinit var adapter: MoviesAdapter
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    @Inject
    lateinit var presenter: FavoriteMoviesPresenter

    override fun getViewContext(): Context? = super.getContext()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerPresentersComponent.builder()
                .appComponent((activity?.application as App).getAppComponent())
                .presentersModule(PresentersModule())
                .build()
                .inject(this)
        presenter.view = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = MoviesAdapter(context!!, this)
        favoriteMovies.adapter = adapter
        val layoutManager = LinearLayoutManager(context)
        favoriteMovies.layoutManager = layoutManager

        presenter.getFavoriteMovies(Constants.SORT_BY, Constants.API_KEY, Constants.SESSION_ID, Constants.ACCOUNT_ID, 0)
        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.getFavoriteMovies(Constants.SORT_BY, Constants.API_KEY, Constants.SESSION_ID, Constants.ACCOUNT_ID, page)
            }
        }
        favoriteMovies.addOnScrollListener(scrollListener)
        ItemTouchHelper(RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)).attachToRecyclerView(favoriteMovies)
    }

    override fun onClick(v: View?) {
        startActivity(ParticularMovieActivity.newIntent(context!!, Constants.MOVIE_ID, v?.tag as Int))
    }

    override fun showFavoriteMovies(movies: List<Movie>) {
        adapter.setResponse(movies)
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, "favorite movies message: $message", Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is MoviesAdapter.ViewHolder) {
            val name = adapter.getList()[viewHolder.adapterPosition].title
            val deletedItem = adapter.getList()[viewHolder.adapterPosition]
            val deletedIndex = viewHolder.adapterPosition
            adapter.removeItem(viewHolder.adapterPosition)
            presenter.markMovieAsFavorite(Constants.ACCOUNT_ID, Constants.API_KEY, Constants.SESSION_ID, viewHolder.itemView.tag as Int, false)

            val snackbar = Snackbar.make(view!!, "$name removed from cart!", Snackbar.LENGTH_LONG)
            snackbar.setAction("UNDO", {
                adapter.restoreItem(deletedItem, deletedIndex)
                presenter.markMovieAsFavorite(Constants.ACCOUNT_ID, Constants.API_KEY, Constants.SESSION_ID, viewHolder.itemView.tag as Int, true)
            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
        }
    }

}
