package com.example.moviereview.network;

import android.content.Context;

import com.example.moviereview.Constants;
import com.example.moviereview.api.AuthenticationApiSet;
import com.example.moviereview.api.FavoriteMoviesApiSet;
import com.example.moviereview.api.MoviesApiSet;
import com.example.moviereview.api.RatedMoviesApiSet;
import com.example.moviereview.api.WatchListMoviesApiSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024;
    private static Retrofit build;

    private static Retrofit getRetrofitInstance(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(120, TimeUnit.SECONDS);
        builder.readTimeout(120, TimeUnit.SECONDS);
        builder.writeTimeout(120, TimeUnit.SECONDS);

        File cacheDir = new File(context.getCacheDir(), "cached");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        builder.cache(cache);
        Gson gson = new GsonBuilder().create();

        if (build == null) {
            RxJava2CallAdapterFactory rxAdapter = RxJava2CallAdapterFactory.create();
            build = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_API_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(rxAdapter)
                    .build();
        }
        return build;
    }

    public static MoviesApiSet getMoviesApiSet(Context context) {
        return ServiceGenerator.getRetrofitInstance(context).create(MoviesApiSet.class);
    }

    public static FavoriteMoviesApiSet getFavoriteMoviesApiSet(Context context) {
        return ServiceGenerator.getRetrofitInstance(context).create(FavoriteMoviesApiSet.class);
    }

    public static RatedMoviesApiSet getRatedMoviesApiSet(Context context) {
        return ServiceGenerator.getRetrofitInstance(context).create(RatedMoviesApiSet.class);
    }

    public static WatchListMoviesApiSet getWatchListMoviesApiSet(Context context) {
        return ServiceGenerator.getRetrofitInstance(context).create(WatchListMoviesApiSet.class);
    }

    public static AuthenticationApiSet getAuthenticationApiSet(Context context) {
        return ServiceGenerator.getRetrofitInstance(context).create(AuthenticationApiSet.class);
    }

}

