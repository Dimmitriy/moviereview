package com.example.moviereview.discover.model

import com.example.moviereview.model.MoviesResponseModel
import io.reactivex.Observable

interface DiscoverRepository {

    fun getDiscoverMovies(sort_by: String, api_key: String, page: Int): Observable<MoviesResponseModel>

}