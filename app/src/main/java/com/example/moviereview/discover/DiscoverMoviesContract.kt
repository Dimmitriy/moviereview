package com.example.moviereview.discover

import com.example.moviereview.base.BasePresenter
import com.example.moviereview.base.BaseView
import com.example.moviereview.model.Movie

class DiscoverMoviesContract {

    interface View : BaseView {

        fun showDiscoverMovies(movies: List<Movie>)

    }

    interface Presenter : BasePresenter {

        fun getDiscoverMovies(sort_by: String, api_key: String, page: Int)

        fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, mediaId: Int, favorite: Boolean)

        fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, mediaId: Int, watchlist: Boolean)

        fun rateMovie(movie_id: Int, api_key: String, session_id: String, value: Float)

    }

}
