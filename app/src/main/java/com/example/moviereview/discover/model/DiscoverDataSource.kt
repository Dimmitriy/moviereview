package com.example.moviereview.discover.model

import com.example.moviereview.api.MoviesApiSet
import com.example.moviereview.model.MoviesResponseModel
import io.reactivex.Observable

class DiscoverDataSource(private val apiSet: MoviesApiSet): DiscoverRepository {

    override fun getDiscoverMovies(sort_by: String, api_key: String, page: Int): Observable<MoviesResponseModel> {
        return apiSet.getDiscoverMovies(sort_by, api_key, page)
    }

}