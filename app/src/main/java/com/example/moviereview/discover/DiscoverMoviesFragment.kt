package com.example.moviereview.discover

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.moviereview.*
import com.example.moviereview.di.component.DaggerPresentersComponent
import com.example.moviereview.di.module.PresentersModule
import com.example.moviereview.model.Movie
import com.example.moviereview.particular.ParticularMovieActivity
import kotlinx.android.synthetic.main.fragment_discover.*
import javax.inject.Inject
import android.support.v7.widget.helper.ItemTouchHelper
import com.example.moviereview.RecyclerItemTouchHelper
import android.support.design.widget.Snackbar

class DiscoverMoviesFragment : Fragment(), DiscoverMoviesContract.View,
        View.OnClickListener, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private lateinit var adapter: MoviesAdapter
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener

    @Inject
    lateinit var presenter: DiscoverMoviesPresenter

    override fun getViewContext(): Context? = super.getContext()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerPresentersComponent.builder()
                .appComponent((activity?.application as App).getAppComponent())
                .presentersModule(PresentersModule())
                .build()
                .inject(this)
        presenter.view = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_discover, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = MoviesAdapter(context!!, this)
        discoverMovies.adapter = adapter
        val layoutManager = LinearLayoutManager(context)
        discoverMovies.layoutManager = layoutManager

        presenter.getDiscoverMovies(Constants.SORT_BY, Constants.API_KEY, 0)
        scrollListener = object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                presenter.getDiscoverMovies(Constants.SORT_BY, Constants.API_KEY, page)
            }
        }
        discoverMovies.addOnScrollListener(scrollListener)
        ItemTouchHelper(RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)).attachToRecyclerView(discoverMovies)
    }

    override fun onClick(v: View?) {
        startActivity(ParticularMovieActivity.newIntent(context!!, Constants.MOVIE_ID, v?.tag as Int))
    }

    override fun showDiscoverMovies(movies: List<Movie>) {
        adapter.setResponse(movies)
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, "discover movies message: $message", Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is MoviesAdapter.ViewHolder) {
            val name = adapter.getList()[viewHolder.adapterPosition].title
            val deletedItem = adapter.getList()[viewHolder.adapterPosition]
            val deletedIndex = viewHolder.adapterPosition
            adapter.removeItem(viewHolder.adapterPosition)

            val snackbar = Snackbar.make(view!!, "$name removed from cart!", Snackbar.LENGTH_LONG)
            snackbar.setAction("UNDO", {
                adapter.restoreItem(deletedItem, deletedIndex)
            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
        }
    }

}
