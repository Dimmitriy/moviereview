package com.example.moviereview.rated.model

import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.RateMovieResponse
import io.reactivex.Observable

interface RatedRepository {

    fun getRatedMovies(account_id: String, sort_by: String, api_key: String, session_id: String, page: Int): Observable<MoviesResponseModel>

    fun rateMovie(movie_id: Int, api_key: String, session_id: String, body: HashMap<String, Any>): Observable<RateMovieResponse>

}