package com.example.moviereview.rated

import com.example.moviereview.base.BasePresenter
import com.example.moviereview.base.BaseView
import com.example.moviereview.model.Movie

class RatedMoviesContract {

    interface View : BaseView {

        fun showRatedMovies(movies: List<Movie>)

    }

    interface Presenter : BasePresenter {

        fun getRatedMovies(sort_by: String, api_key: String, session_id: String, account_id: String, page: Int)

        fun rateMovie(movie_id: Int, api_key: String, session_id: String, value: Float)

    }

}
