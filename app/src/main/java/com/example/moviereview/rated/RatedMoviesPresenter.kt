package com.example.moviereview.rated

import com.example.moviereview.rated.model.RatedRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RatedMoviesPresenter
@Inject constructor(private val ratedRepository: RatedRepository) : RatedMoviesContract.Presenter {

    lateinit var view: RatedMoviesContract.View
    private val disposable = CompositeDisposable()

    override fun getRatedMovies(sort_by: String, api_key: String, session_id: String, account_id: String, page: Int) {
        disposable.add(
                ratedRepository.getRatedMovies(account_id, sort_by, api_key, session_id, page + 1)
                        .compose(adjustThreads(view))
                        .subscribe({ moviesResponseModel ->
                            view.showRatedMovies(moviesResponseModel.results)
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun rateMovie(movie_id: Int, api_key: String, session_id: String, value: Float) {
        val body = HashMap<String, Any>()
        body["value"] = value
        disposable.add(
                ratedRepository.rateMovie(movie_id, api_key, session_id, body)
                        .compose(adjustThreads(view))
                        .subscribe({
                            view.showMessage("movie rated code: ${it.status_code}, message: ${it.status_message}")
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {
        disposable.dispose()
    }

}