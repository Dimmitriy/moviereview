package com.example.moviereview.rated.model

import com.example.moviereview.api.RatedMoviesApiSet
import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.RateMovieResponse
import io.reactivex.Observable

class RatedDataSource(private val apiSet: RatedMoviesApiSet): RatedRepository {

    override fun getRatedMovies(account_id: String, sort_by: String, api_key: String, session_id: String, page: Int): Observable<MoviesResponseModel> {
        return apiSet.getRatedMovies(account_id, sort_by, api_key, session_id, page)
    }

    override fun rateMovie(movie_id: Int, api_key: String, session_id: String, body: HashMap<String, Any>): Observable<RateMovieResponse> {
        return apiSet.rateMovie(movie_id, api_key, session_id, body)
    }

}