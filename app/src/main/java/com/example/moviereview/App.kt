package com.example.moviereview

import android.app.Application
import com.example.moviereview.di.module.AppModule

import com.example.moviereview.di.component.AppComponent
import com.example.moviereview.di.component.DaggerAppComponent

class App : Application() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    fun getAppComponent(): AppComponent {
        return appComponent
    }

}