package com.example.moviereview.particular

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.moviereview.App
import com.example.moviereview.Constants
import com.example.moviereview.R
import com.example.moviereview.TextFormatUtils
import com.example.moviereview.di.component.DaggerPresentersComponent
import com.example.moviereview.di.module.PresentersModule
import com.example.moviereview.model.ParticularResponseModel
import kotlinx.android.synthetic.main.fragment_particular_movie.*
import javax.inject.Inject
import com.jakewharton.rxbinding2.widget.RxTextView

class ParticularMovieFragment : Fragment(), ParticularMovieContract.View, View.OnClickListener {

    private var isDataLoaded: Boolean = false
    private var noData: String? = null
    private var moviesId: Int = 0
    private lateinit var textUtils: TextFormatUtils

    @Inject
    lateinit var presenter: ParticularMoviePresenter

    override fun getViewContext(): Context? = super.getContext()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerPresentersComponent.builder()
                .appComponent((activity?.application as App).getAppComponent())
                .presentersModule(PresentersModule())
                .build()
                .inject(this)
        presenter.view = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_particular_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowHomeEnabled(true)

        if (arguments != null) {
            moviesId = arguments!!.getInt(Constants.MOVIE_ID)
        }
        noData = resources.getString(R.string.no_data)
        presenter.getParticularMovie(moviesId, Constants.API_KEY)
        textUtils = TextFormatUtils()
        fab_favorite.setOnClickListener(this)
        fab_rating.setOnClickListener(this)
        fab_watchlist.setOnClickListener(this)
    }

    override fun showParticularMovie(particularMovie: ParticularResponseModel) {
        isDataLoaded = true
        textOriginalTitle!!.text = particularMovie.originalTitle
        textHomepage!!.text = particularMovie.homepage
        textReleaseDate!!.text = particularMovie.releaseDate
        textStatus!!.text = particularMovie.status
        textTagLine!!.text = particularMovie.tagline
        textVoteAverage!!.text = (particularMovie.voteAverage).toString()
        textVoteCount!!.text = (particularMovie.voteCount).toString()

        val listCountries = particularMovie.productionCountries
        if (listCountries.size > 0)
            textProductionCountries!!.text = textUtils.getString(listCountries)
        val listCompanies = particularMovie.productionCompanies
        if (listCompanies.size > 0)
            textProductionCompanies!!.text = textUtils.getString(listCompanies)
        val listLanguages = particularMovie.spokenLanguages
        if (listLanguages.size > 0)
            textSpokenLanguages!!.text = textUtils.getString(listLanguages)

        Glide
                .with(context!!)
                .load(Constants.IMG_LOAD_NEWS + particularMovie.backdropPath)
                .into(imgPoster!!)
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, "particular movie message: $message", Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progress!!.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress!!.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId) {
            android.R.id.home ->
                NavUtils.navigateUpFromSameTask(activity!!)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab_favorite -> presenter.markMovieAsFavoriteClicked()
            R.id.fab_watchlist -> presenter.markMovieToWatchListClicked()
            R.id.fab_rating -> presenter.rateMovieClicked()
        }
    }

    override fun markMovieAsFavorite(id: Int) {
        presenter.markMovieAsFavorite(Constants.ACCOUNT_ID, Constants.API_KEY, Constants.SESSION_ID, id, true)
    }

    override fun markMovieToWatchList(id: Int) {
        presenter.markMovieToWatchList(Constants.ACCOUNT_ID, Constants.API_KEY, Constants.SESSION_ID, id, true)
    }

    override fun rateMovie(id: Int) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.dialog_movie_rate)
        val movieRate = dialog.findViewById(R.id.rate) as EditText
        val confirmRate = dialog.findViewById(R.id.confirm_rate) as Button

        RxTextView.textChanges(movieRate)
                .subscribe({
                    confirmRate.isEnabled = it.isNotEmpty()
                            && it.toString().toFloat() >= 0.5
                            && it.toString().toFloat() <= 10
                })

        confirmRate.setOnClickListener({
            val rate = movieRate.text.toString().toFloat()
            presenter.rateMovie(id, Constants.API_KEY, Constants.SESSION_ID, rate)
            dialog.dismiss()
        })
        dialog.show()
    }

}
