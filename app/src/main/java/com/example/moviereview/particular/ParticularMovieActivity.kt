package com.example.moviereview.particular

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.moviereview.Constants
import com.example.moviereview.R
import kotlinx.android.synthetic.main.activity_particular_movie.*

class ParticularMovieActivity : AppCompatActivity() {

    companion object {
        fun newIntent(context: Context, key: String, value: Int): Intent {
            val intent = Intent(context, ParticularMovieActivity::class.java)
            intent.putExtra(key, value)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_particular_movie)

        val bundle = Bundle()
        bundle.putInt(Constants.MOVIE_ID, intent.extras[Constants.MOVIE_ID] as Int)
        val fragment = ParticularMovieFragment()
        fragment.arguments = bundle
        supportFragmentManager
                .beginTransaction()
                .add(R.id.container, fragment)
                .commit()
    }

}
