package com.example.moviereview.particular.model

import com.example.moviereview.api.MoviesApiSet
import com.example.moviereview.model.ParticularResponseModel
import io.reactivex.Observable

class ParticularDataSource(private val apiSet: MoviesApiSet): ParticularRepository {

    override fun getParticularMovie(id: Int, apiKey: String): Observable<ParticularResponseModel> {
        return apiSet.getParticularMovie(id, apiKey)
    }
}