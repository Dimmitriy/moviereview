package com.example.moviereview.particular

import com.example.moviereview.base.BasePresenter
import com.example.moviereview.base.BaseView
import com.example.moviereview.model.ParticularResponseModel

class ParticularMovieContract {

    interface View : BaseView {

        fun showParticularMovie(particularMovie: ParticularResponseModel)

        fun markMovieAsFavorite(id: Int)

        fun markMovieToWatchList(id: Int)

        fun rateMovie(id: Int)

    }

    interface Presenter : BasePresenter {

        fun getParticularMovie(id: Int, api_key: String)

        fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, mediaId: Int, watchlist: Boolean)

        fun markMovieAsFavorite(account_id: String, api_key: String, session_id: String, mediaId: Int, favorite: Boolean)

        fun rateMovie(movie_id: Int, api_key: String, session_id: String, value: Float)

        fun markMovieAsFavoriteClicked()

        fun markMovieToWatchListClicked()

        fun rateMovieClicked()

    }
}
