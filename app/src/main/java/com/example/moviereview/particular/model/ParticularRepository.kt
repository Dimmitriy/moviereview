package com.example.moviereview.particular.model

import com.example.moviereview.model.ParticularResponseModel
import io.reactivex.Observable

interface ParticularRepository {

    fun getParticularMovie(id: Int, apiKey: String): Observable<ParticularResponseModel>

}