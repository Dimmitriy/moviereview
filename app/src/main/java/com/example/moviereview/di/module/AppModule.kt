package com.example.moviereview.di.module

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.example.moviereview.Constants
import com.example.moviereview.api.FavoriteMoviesApiSet
import com.example.moviereview.api.MoviesApiSet
import com.example.moviereview.api.RatedMoviesApiSet
import com.example.moviereview.api.WatchListMoviesApiSet
import com.example.moviereview.discover.model.DiscoverDataSource
import com.example.moviereview.discover.model.DiscoverRepository
import com.example.moviereview.favorite.model.FavoriteDataSource
import com.example.moviereview.favorite.model.FavoriteRepository
import com.example.moviereview.particular.model.ParticularDataSource
import com.example.moviereview.particular.model.ParticularRepository
import com.example.moviereview.rated.model.RatedDataSource
import com.example.moviereview.rated.model.RatedRepository
import com.example.moviereview.search.model.SearchDataSource
import com.example.moviereview.search.model.SearchRepository
import com.example.moviereview.watchlist.model.WatchListDataSource
import com.example.moviereview.watchlist.model.WatchListRepository
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule(private val appContext: Context) {

    /**
     * 50MB cache size.
     */
    private val DISK_CACHE_SIZE = 50 * 1024 * 1024

    @Singleton
    @Provides
    fun provideContext(): Context = appContext

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("com.example.moviereview", MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideRetrofit(context: Context): Retrofit {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(120, TimeUnit.SECONDS)
        builder.readTimeout(120, TimeUnit.SECONDS)
        builder.writeTimeout(120, TimeUnit.SECONDS)

        val cacheDir = File(context.cacheDir, "cached")
        val cache = Cache(cacheDir, DISK_CACHE_SIZE.toLong())
        builder.cache(cache)
        val gson = GsonBuilder().create()

        return Retrofit.Builder()
                .baseUrl(Constants.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(builder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    internal fun provideFavoriteDataSource(retrofit: Retrofit): FavoriteRepository {
        return FavoriteDataSource(retrofit.create(FavoriteMoviesApiSet::class.java))
    }

    @Provides
    @Singleton
    internal fun provideDiscoverDataSource(retrofit: Retrofit): DiscoverRepository {
        return DiscoverDataSource(retrofit.create(MoviesApiSet::class.java))
    }

    @Provides
    @Singleton
    internal fun provideSearchDataSource(retrofit: Retrofit): SearchRepository {
        return SearchDataSource(retrofit.create(MoviesApiSet::class.java))
    }

    @Provides
    @Singleton
    internal fun provideRatedDataSource(retrofit: Retrofit): RatedRepository {
        return RatedDataSource(retrofit.create(RatedMoviesApiSet::class.java))
    }

    @Provides
    @Singleton
    internal fun provideWatchListDataSource(retrofit: Retrofit): WatchListRepository {
        return WatchListDataSource(retrofit.create(WatchListMoviesApiSet::class.java))
    }

    @Provides
    @Singleton
    internal fun provideParticularDataSource(retrofit: Retrofit): ParticularRepository {
        return ParticularDataSource(retrofit.create(MoviesApiSet::class.java))
    }

}
