package com.example.moviereview.di.module;

import android.content.Context;
import com.example.moviereview.di.scopes.Scope;
import com.example.moviereview.di.scopes.Scopes;
import com.example.moviereview.discover.DiscoverMoviesPresenter;
import com.example.moviereview.discover.model.DiscoverDataSource;
import com.example.moviereview.favorite.FavoriteMoviesPresenter;
import com.example.moviereview.favorite.model.FavoriteDataSource;
import com.example.moviereview.network.ServiceGenerator;
import com.example.moviereview.particular.ParticularMoviePresenter;
import com.example.moviereview.particular.model.ParticularDataSource;
import com.example.moviereview.rated.RatedMoviesPresenter;
import com.example.moviereview.rated.model.RatedDataSource;
import com.example.moviereview.search.SearchMoviesPresenter;
import com.example.moviereview.search.model.SearchDataSource;
import com.example.moviereview.watchlist.WatchListMoviesPresenter;
import com.example.moviereview.watchlist.model.WatchListDataSource;

import dagger.Module;
import dagger.Provides;

@Module
public final class PresentersModule {

    @Provides
    @Scope(Scopes.VIEW)
    FavoriteMoviesPresenter provideFavoriteMoviesPresenter(Context context) {
        return new FavoriteMoviesPresenter(
                new FavoriteDataSource(ServiceGenerator.getFavoriteMoviesApiSet(context)));
    }

    @Provides
    @Scope(Scopes.VIEW)
    DiscoverMoviesPresenter provideDiscoverMoviesPresenter(Context context) {
        return new DiscoverMoviesPresenter(
                new FavoriteDataSource(ServiceGenerator.getFavoriteMoviesApiSet(context)),
                new RatedDataSource(ServiceGenerator.getRatedMoviesApiSet(context)),
                new WatchListDataSource(ServiceGenerator.getWatchListMoviesApiSet(context)),
                new DiscoverDataSource(ServiceGenerator.getMoviesApiSet(context)));
    }

    @Provides
    @Scope(Scopes.VIEW)
    RatedMoviesPresenter provideRatedMoviesPresenter(Context context) {
        return new RatedMoviesPresenter(
                new RatedDataSource(ServiceGenerator.getRatedMoviesApiSet(context)));
    }

    @Provides
    @Scope(Scopes.VIEW)
    SearchMoviesPresenter provideSearchMoviesPresenter(Context context) {
        return new SearchMoviesPresenter(
                new FavoriteDataSource(ServiceGenerator.getFavoriteMoviesApiSet(context)),
                new RatedDataSource(ServiceGenerator.getRatedMoviesApiSet(context)),
                new WatchListDataSource(ServiceGenerator.getWatchListMoviesApiSet(context)),
                new SearchDataSource(ServiceGenerator.getMoviesApiSet(context)));
    }

    @Provides
    @Scope(Scopes.VIEW)
    WatchListMoviesPresenter provideWatchListMoviesPresenter(Context context) {
        return new WatchListMoviesPresenter(
                new WatchListDataSource(ServiceGenerator.getWatchListMoviesApiSet(context)));
    }

    @Provides
    @Scope(Scopes.VIEW)
    ParticularMoviePresenter provideParticularMoviePresenter(Context context) {
        return new ParticularMoviePresenter(
                new FavoriteDataSource(ServiceGenerator.getFavoriteMoviesApiSet(context)),
                new RatedDataSource(ServiceGenerator.getRatedMoviesApiSet(context)),
                new WatchListDataSource(ServiceGenerator.getWatchListMoviesApiSet(context)),
                new ParticularDataSource(ServiceGenerator.getMoviesApiSet(context)));
    }


}
