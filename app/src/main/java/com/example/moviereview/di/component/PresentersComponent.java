package com.example.moviereview.di.component;

import com.example.moviereview.di.module.PresentersModule;
import com.example.moviereview.di.scopes.Scope;
import com.example.moviereview.di.scopes.Scopes;
import com.example.moviereview.discover.DiscoverMoviesFragment;
import com.example.moviereview.favorite.FavoriteMoviesFragment;
import com.example.moviereview.particular.ParticularMovieFragment;
import com.example.moviereview.rated.RatedMoviesFragment;
import com.example.moviereview.search.SearchMoviesFragment;
import com.example.moviereview.watchlist.WatchListMoviesFragment;

import dagger.Component;

@Scope(Scopes.VIEW)
@Component(
        modules = {PresentersModule.class},
        dependencies = {AppComponent.class}
)
public interface PresentersComponent {

    void inject(FavoriteMoviesFragment view);

    void inject(DiscoverMoviesFragment view);

    void inject(ParticularMovieFragment view);

    void inject(SearchMoviesFragment view);

    void inject(RatedMoviesFragment view);

    void inject(WatchListMoviesFragment view);

}
