package com.example.moviereview.di.component

import android.content.Context
import android.content.SharedPreferences
import com.example.moviereview.di.module.AppModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {

    fun retrofit(): Retrofit

    fun context(): Context

    fun sharedPreferences(): SharedPreferences

}