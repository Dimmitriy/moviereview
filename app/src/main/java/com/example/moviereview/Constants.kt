package com.example.moviereview

object Constants {
    const val BASE_API_URL = "https://api.themoviedb.org/3/"
    const val SORT_BY = "popularity.desc"
    const val API_KEY = "220b0493fbeae7d0273abdd06b847bf9"
    const val IMG_LOAD_NEWS = "https://image.tmdb.org/t/p/w250_and_h141_bestv2"
    const val MOVIE_ID = "MOVIE_ID"

    // temporary used while authentication not implemented
    const val SESSION_ID = "14f96b850dc35fd92a36b976607a987cd365b2c5"
    const val ACCOUNT_ID = "7930992"
}
