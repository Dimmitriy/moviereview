package com.example.moviereview.watchlist.model

import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.MarkToWatchListResponse
import io.reactivex.Observable

interface WatchListRepository {

    fun getWatchListMovies(account_id: String, sort_by: String, api_key: String, session_id: String, page: Int): Observable<MoviesResponseModel>

    fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, body: HashMap<String, Any>): Observable<MarkToWatchListResponse>

}