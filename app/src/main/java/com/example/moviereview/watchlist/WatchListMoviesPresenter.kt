package com.example.moviereview.watchlist

import com.example.moviereview.watchlist.model.WatchListRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class WatchListMoviesPresenter
@Inject constructor(private val watchListRepository: WatchListRepository): WatchListMoviesContract.Presenter {

    lateinit var view: WatchListMoviesContract.View
    private val disposable = CompositeDisposable()

    override fun getWatchListMovies(sort_by: String, api_key: String, session_id: String, account_id: String, page: Int) {
        disposable.add(
                watchListRepository.getWatchListMovies(account_id, sort_by, api_key, session_id, page + 1)
                        .compose(adjustThreads(view))
                        .subscribe({ moviesResponseModel ->
                            view.showWatchListMovies(moviesResponseModel.results)
                        }, {throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, mediaId: Int, watchlist: Boolean) {
        val body = HashMap<String, Any>()
        body["media_type"] = "movie"
        body["media_id"] = mediaId
        body["watchlist"] = watchlist
        disposable.add(
                watchListRepository.markMovieToWatchList(account_id, api_key, session_id, body)
                        .compose(adjustThreads(view))
                        .subscribe({
                            view.showMessage("movie marked to watchlist code: ${it.status_code}, message: ${it.status_message}")
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {
        disposable.dispose()
    }

}