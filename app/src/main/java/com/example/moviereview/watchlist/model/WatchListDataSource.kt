package com.example.moviereview.watchlist.model

import com.example.moviereview.api.WatchListMoviesApiSet
import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.MarkToWatchListResponse
import io.reactivex.Observable

class WatchListDataSource(private val watchListApiSet: WatchListMoviesApiSet): WatchListRepository {

    override fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, body: HashMap<String, Any>): Observable<MarkToWatchListResponse> {
        return watchListApiSet.markMovieToWatchList(account_id, api_key, session_id, body)
    }

    override fun getWatchListMovies(account_id: String, sort_by: String, api_key: String, session_id: String, page: Int): Observable<MoviesResponseModel> {
        return watchListApiSet.getWatchListMovies(account_id, sort_by, api_key, session_id, page)
    }

}