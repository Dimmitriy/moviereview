package com.example.moviereview.watchlist

import com.example.moviereview.base.BasePresenter
import com.example.moviereview.base.BaseView
import com.example.moviereview.model.Movie
import retrofit2.http.Query

class WatchListMoviesContract {

    interface View : BaseView {

        fun showWatchListMovies(movies: List<Movie>)

    }

    interface Presenter : BasePresenter {

        fun getWatchListMovies(sort_by: String, api_key: String, session_id: String, account_id: String, page: Int)

        fun markMovieToWatchList(account_id: String, api_key: String, session_id: String, mediaId: Int, watchlist: Boolean)

    }

}
