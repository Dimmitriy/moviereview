package com.example.moviereview.api

import com.example.moviereview.model.response.CreateSessionResponse
import com.example.moviereview.model.response.GetAccountResponse
import com.example.moviereview.model.response.RequestTokenResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface AuthenticationApiSet {

    @GET("authentication/token/new")
    fun getRequestToken(@Query("api_key") api_key: String): Observable<RequestTokenResponse>

    @GET("authentication/session/new")
    fun createSession(@Query("api_key") api_key: String,
                      @Query("request_token") request_token: String): Observable<CreateSessionResponse>

    @GET("authentication/session/new")
    fun getAccount(@Query("api_key") api_key: String,
                   @Query("session_id") session_id: String): Observable<GetAccountResponse>

}