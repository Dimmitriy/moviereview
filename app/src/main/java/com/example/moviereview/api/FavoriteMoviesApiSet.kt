package com.example.moviereview.api

import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.MarkToFavoriteResponse
import io.reactivex.Observable
import retrofit2.http.*

interface FavoriteMoviesApiSet {

    @GET("account/{account_id}/favorite/movies")
    fun getFavoriteMovies(@Path("account_id") account_id: String,
                          @Query("sort_by") sort_by: String,
                          @Query("api_key") api_key: String,
                          @Query("session_id") session_id: String,
                          @Query("page") page: Int): Observable<MoviesResponseModel>

    @Headers("Content-Type: application/json;charset=utf-8")
    @POST("account/{account_id}/favorite")
    fun markMovieAsFavorite(@Path("account_id") account_id: String,
                            @Query("api_key") api_key: String,
                            @Query("session_id") session_id: String,
                            @Body body: HashMap<String, Any>): Observable<MarkToFavoriteResponse>

}