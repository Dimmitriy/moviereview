package com.example.moviereview.api

import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.response.RateMovieResponse
import io.reactivex.Observable
import retrofit2.http.*

interface RatedMoviesApiSet {

    @GET("account/{account_id}/rated/movies")
    fun getRatedMovies(@Path("account_id") account_id: String,
                       @Query("sort_by") sort_by: String,
                       @Query("api_key") api_key: String,
                       @Query("session_id") session_id: String,
                       @Query("page") page: Int): Observable<MoviesResponseModel>

    @Headers("Content-Type: application/json;charset=utf-8")
    @POST("movie/{movie_id}/rating")
    fun rateMovie(@Path("movie_id") movie_id: Int,
                  @Query("api_key") api_key: String,
                  @Query("session_id") session_id: String,
                  @Body body: HashMap<String, Any>): Observable<RateMovieResponse>

}