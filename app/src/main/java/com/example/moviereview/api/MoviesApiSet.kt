package com.example.moviereview.api

import com.example.moviereview.model.MoviesResponseModel
import com.example.moviereview.model.ParticularResponseModel

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesApiSet {

    @GET("discover/movie")
    fun getDiscoverMovies(@Query("sort_by") sort_by: String,
                          @Query("api_key") api_key: String,
                          @Query("page") page: Int): Observable<MoviesResponseModel>

    @GET("search/movie")
    fun getSearchMovies(@Query("query") query: String,
                        @Query("api_key") api_key: String): Observable<MoviesResponseModel>

    @GET("movie/{id}")
    fun getParticularMovie(@Path("id") id: Int,
                           @Query("api_key") api_key: String): Observable<ParticularResponseModel>

}
