package com.example.moviereview

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.moviereview.model.Movie
import java.util.ArrayList

class MoviesAdapter(context: Context, private val listener: View.OnClickListener) :
        RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private var radius = context.resources.getDimension(R.dimen.corner_radius_medium).toInt()
    private val glide = Glide.with(context)
    private val list = ArrayList<Movie>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movies, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTitle.text = list[position].title
        holder.tvPopularity.text = list[position].popularity.toString()
        holder.tvOverview.text = list[position].overview
        holder.tvVotesCount.text = list[position].voteCount.toString()
        holder.view.tag = list[position].id
        if (!TextUtils.isEmpty(list[position].posterPath)) {
            val loadUrl = Constants.IMG_LOAD_NEWS + list[position].posterPath
            glide.load(loadUrl)
                    .apply(RequestOptions().transforms(CenterCrop(), RoundedCorners(radius)))
                    .into(holder.imgPoster)
        }
        holder.view.setOnClickListener {
            listener.onClick(holder.view)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setResponse(results: List<Movie>) {
        list.addAll(results)
        notifyDataSetChanged()
    }

    fun getList() = list

    fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: Movie, position: Int) {
        list.add(position, item)
        notifyItemInserted(position)
    }

    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var tvVotesCount = view.findViewById(R.id.tvVotesCount) as TextView
        var tvTitle = view.findViewById(R.id.tvTitleMessage) as TextView
        var tvPopularity = view.findViewById(R.id.tvPopularity) as TextView
        var tvOverview = view.findViewById(R.id.tvOverview) as TextView
        var imgPoster = view.findViewById(R.id.imgPoster) as ImageView
        var cvReminderMovies = view.findViewById(R.id.cvReminderMovies) as CardView
    }

}
