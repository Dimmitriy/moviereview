package com.example.moviereview.main

import com.example.moviereview.base.BasePresenter
import com.example.moviereview.base.BaseView
import retrofit2.http.Query

class MainContract {

    interface View : BaseView

    interface Presenter : BasePresenter {

        fun authenticate(@Query("api_key") api_key: String)

    }

}
