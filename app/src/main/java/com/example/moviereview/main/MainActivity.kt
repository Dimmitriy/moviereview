package com.example.moviereview.main

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import com.example.moviereview.BottomNavigationViewHelper
import com.example.moviereview.R
import com.example.moviereview.discover.DiscoverMoviesFragment
import com.example.moviereview.favorite.FavoriteMoviesFragment
import com.example.moviereview.model.Movie
import com.example.moviereview.rated.RatedMoviesFragment
import com.example.moviereview.search.SearchMoviesFragment
import com.example.moviereview.watchlist.WatchListMoviesFragment
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View {

    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var fragments: Array<Fragment>

    @Inject
    lateinit var presenter: MainPresenter

    override fun getViewContext(): Context = applicationContext

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        Injector.getAppComponent()!!.inject(this)
//        presenter.view = this
//        presenter.authenticate(Constants.API_KEY)

        setSupportActionBar(toolbar)

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            fragmentTransaction = supportFragmentManager.beginTransaction()
            when (item.itemId) {
                R.id.tab_discover -> fragmentTransaction.replace(R.id.container, fragments[0])
                R.id.tab_search -> fragmentTransaction.replace(R.id.container, fragments[1])
                R.id.tab_rated -> fragmentTransaction.replace(R.id.container, fragments[2])
                R.id.tab_favorites -> fragmentTransaction.replace(R.id.container, fragments[3])
                R.id.tab_watchlist -> fragmentTransaction.replace(R.id.container, fragments[4])
            }
            fragmentTransaction.commit()
            true
        }
        BottomNavigationViewHelper.disableShiftMode(bottom_navigation)
        val discoverMoviesFragment = DiscoverMoviesFragment()
        val searchMoviesFragment = SearchMoviesFragment()
        val ratedMoviesFragment = RatedMoviesFragment()
        val favoriteMoviesFragment = FavoriteMoviesFragment()
        val watchListMoviesFragment = WatchListMoviesFragment()
        fragments = arrayOf(discoverMoviesFragment, searchMoviesFragment,
                ratedMoviesFragment, favoriteMoviesFragment, watchListMoviesFragment)
        bottom_navigation.selectedItemId = R.id.tab_discover
    }

    override fun showMessage(message: String) {

    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

}
