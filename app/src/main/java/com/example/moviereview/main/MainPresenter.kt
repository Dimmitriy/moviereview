package com.example.moviereview.main

import com.example.moviereview.network.ServiceGenerator
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainPresenter
@Inject constructor(): MainContract.Presenter {

    lateinit var view: MainContract.View
    private val apiSet = ServiceGenerator.getAuthenticationApiSet(view.getViewContext())
    private val disposable = CompositeDisposable()

    override fun authenticate(api_key: String) {
        disposable.add(
                apiSet.getRequestToken(api_key)
                        .compose(adjustThreads(view))
                        .subscribe({ moviesResponseModel ->
                            getSessionId(api_key, moviesResponseModel.requestToken)
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    private fun getSessionId(api_key: String, request_token: String) {
        disposable.add(
                apiSet.createSession(api_key, request_token)
                        .compose(adjustThreads(view))
                        .subscribe({ moviesResponseModel ->
                            getAccount(api_key, moviesResponseModel.sessionId)
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    private fun getAccount(api_key: String, sessionId: String) {
        disposable.add(
                apiSet.getAccount(api_key, sessionId)
                        .compose(adjustThreads(view))
                        .subscribe({ moviesResponseModel ->
                            view.showMessage(moviesResponseModel.account_id)
                        }, { throwable ->
                            view.showMessage(throwable.message!!)
                        })
        )
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {
        disposable.dispose()
    }

}